# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models
from rest_framework import serializers
from dbproductionApp.models import *

class ArticleSerializer(serializers.ModelSerializer):
    id = models.AutoField(primary_key=True)
    code_barre_texte = CodeBarreSerializer(many=False)
    colis = models.ForeignKey('ColisSerializer', db_column='colis_Id', blank=True, null=True)  # Field name made lowercase.
    firmware = models.ForeignKey('ProduitSerializer', blank=True, null=True)
    lots_origine_lot_ar = LotArticlesSerializer(many=False)
    produit = ProduitSerializer(many=False)
    lot_origine_lot_ar = LotArticlesSerializer(many=False)

    class Meta:
      
        model = Article
        fields = ('id',
                  'code_barre_texte',
                  'colis',
                  'firmware',
                  'lots_origine_lot_ar',
                  'produit', )
        read_only_fields = ('id', )


class CategorieSerializer(serializers.ModelSerializer):
    class Meta:
      
        model = Categorie
        fields = ('code',
                  'nom_en',
                  'nom_fr',
                  )


class CodeBarreSerializer(serializers.ModelSerializer):

    class Meta:
      
        model = CodeBarre
        fields = ('texte',
                  'format',)


class ColisSerializer(serializers.ModelSerializer):
    expedition_expedition = ExpeditionSerializer(many=False)

    class Meta:
      
        model = Colis
        fields = ('expedition_expedition', )
        read_only_fields = ('expedition_expedition', )


class CompositionSerializer(serializers.ModelSerializer):

    produit_fini = ProduitSerializer(many=False)
    matiere_premiere = ProduitSerializer(many=False)

    class Meta:
      
        model = 'composition'
        fields = ('quantite', 'produit_fini', 'matiere_premiere', )
        read_only_fields = ('produit_fini', 'matiere_premiere', )


class EmployeSerializer(serializers.ModelSerializer):

    class Meta:
      
        model = Employe
        fields = ('id', 'active', 'courriel', 'prenom', 'nom', 'telephone',  )
        read_only_fields = ('id',  )

class EvaluationArticleSerializer(serializers.ModelSerializer):
    evaluation_article = InterventionSerializer(many=False)
    article = Article(many=False)

    class Meta:
      
        model = EvaluationArticle
        fields = ('evaluation_article', 'article',  )
        read_only_fields = ('evaluation_article',  )

class EvaluationLotSerializer(serializers.ModelSerializer):
    evaluation_lot = InterventionSerializer(many=False)

    class Meta:
      
        model = EvaluationLot
        fields = ('evaluation_lot',  )
        read_only_fields = ('evaluation_lot',  )


class EvaluationReceptionSerializer(serializers.ModelSerializer):
    evaluation_reception = InterventionSerializer(many=False)

    class Meta:
      
        model = EvaluationReception
        fields = ('evaluation_reception',  )
        read_only_fields = ('evaluation_reception',  )



class ExpeditionSerializer(serializers.ModelSerializer):

    expedition = InterventionSerializer(many=False)

    class Meta:
      
        model = Expedition
        fields = ('commande', 'expedition',  )
        read_only_fields = ('commande',  )


class FournisseurSerializer(serializers.ModelSerializer):

    class Meta:
      
        model = Fournisseur
        fields = ('id', 'contact', 'nom', 'telephone', 'web_site', )
        read_only_fields = ('evaluation_reception',  )


class ImpressionSerializer(serializers.ModelSerializer):
    impression = InterventionSerializer(many=False)

    class Meta:
      
        model = Impression
        fields = ('impression',  )
        read_only_fields = ('impression',  )


class ImpressionCodeBarresSerializer(serializers.ModelSerializer):
    impression_impression = ImpressionSerializer(many=False)
    code_barres_texte = CodeBarreSerializer(many=False)
    class Meta:
      
        model = ImpressionCodeBarres
        fields = ('impression_impression',  'code_barres_texte', )
        read_only_fields = ('impression_impression', 'code_barres_texte', )


class InterventionSerializer(serializers.ModelSerializer):

    employe = EmployeSerializer(many=False)
    lot = LotSerializer(many=False)

    class Meta:
      
        model = 'intervention'
        fields = ('intervention_id',  'date','note',
                   'reussie','employe','lot',)
        read_only_fields = ('intervention_id',  )


class LotSerializer(serializers.ModelSerializer):

    code_barre_texte = CodeBarreSerializer(many=False)
    employe = EmployeSerializer(many=False)
    produit = ProduitSerializer(many=False)

    class Meta:
      
        model = Lot
        fields = ('id', 'type_lot', 'date_creation', 'quantite_courante',
                   'quantite_initial', 'quantite_rejetee', 'code_barre_texte', 'employe', 'produit',
                   )
        read_only_fields = ('id',  )


class LotArticlesSerializer(serializers.ModelSerializer):
    lot_ar = LotSerializer(many=False)
    production_production = ProductionSerializer(many=False)

    class Meta:
      
        model = LotArticles
        fields = ('lot_ar', 'production_production',  )
        #read_only_fields = ('lot_ar',  )


class LotArticlesListArticlesSerializer(serializers.ModelSerializer):
    lot_articles_lot_ar = LotArticlesSerializer(many=True)
    list_articles = ArticleSerializer(many=True)

    class Meta:
      
        model = 'lot_articles_list_articles'
        fields = ('lot_articles_lot_ar', 'list_articles',  )
        #read_only_fields = ('evaluation_reception',  )


class LotMatieresPremieresSerializer(serializers.ModelSerializer):

    evaluation_evaluationlot = EvaluationLotSerializer(many=False)
    reception_reception = ReceptionSerializer(many=False)

    class Meta:
      
        model = LotMatieresPremieres
        fields = ('quantite_commande', 'lot_mp','evaluation_evaluationlot','reception_reception',  )
        read_only_fields = ('lot_mp',  )


class NotificationSerializer(serializers.ModelSerializer):
    notification = InterventionSerializer(many=False)

    class Meta:
      
        model = Notification
        fields = ('notification',  )
        #read_only_fields = ('notification',  )


class ProductionSerializer(serializers.ModelSerializer):
    production = InterventionSerializer(many=False)

    class Meta:
      
        model = ProductionSerializer
        fields = ('production',  )
        # read_only_fields = ('evaluation_reception',  )


class ProduitSerializer(serializers.ModelSerializer):

    fournisseur = FournisseurSerializer(many= False)

    class Meta:
      
        model = Produit
        fields = ('id',
                   'code',
                   'description_en',
                   'description_fr',
                   'nom_en',
                   'nom_fr',
                   'part_number',
                   'quantite_stock',
                   'categorie_code',
                   'fournisseur',
                   )
        read_only_fields = ('id',  )


class ReceptionSerializer(serializers.ModelSerializer):
    reception = InterventionSerializer(many=False)

    class Meta:
      
        model = Reception
        fields = ('reception',  )

