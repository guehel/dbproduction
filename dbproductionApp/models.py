# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class Article(models.Model):
    id = models.AutoField(primary_key=True)
    code_barre_texte = models.ForeignKey('CodeBarre', db_column='code_barre_texte', blank=True, null=True)
    colis = models.ForeignKey('Colis', db_column='colis_Id', blank=True, null=True)  # Field name made lowercase.
    firmware = models.ForeignKey('Produit', blank=True, null=True)
    lots_origine_lot_ar = models.ForeignKey('LotArticles', db_column='lots_origine_lot_Ar_id', blank=True, null=True)  # Field name made lowercase.
    produit = models.ForeignKey('Produit', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'article'


class Categorie(models.Model):
    code = models.CharField(primary_key=True, max_length=255)
    nom_en = models.CharField(max_length=255, blank=True, null=True)
    nom_fr = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'categorie'


class CodeBarre(models.Model):
    texte = models.CharField(primary_key=True, max_length=255)
    format = models.CharField(blank=True, null=True, max_length=255)

    class Meta:
        managed = True
        db_table = 'code_barre'


class Colis(models.Model):
    id = models.AutoField(primary_key=True)
    longueur = models.FloatField()
    note = models.CharField(max_length=255, blank=True, null=True)
    tracking_number = models.CharField(max_length=255, blank=True, null=True)
    hauteur = models.FloatField()
    largeur = models.FloatField()
    expedition_expedition = models.ForeignKey('Expedition', db_column='expedition_Expedition_id', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'colis'


class Composition(models.Model):
    quantite = models.IntegerField()
    produit_fini = models.ForeignKey('Produit')
    matiere_premiere = models.ForeignKey('Produit')

    class Meta:
        managed = True
        db_table = 'composition'
        unique_together = (('produit_fini_id', 'matiere_premiere_id'),)



class Employe(models.Model):
    id = models.AutoField(primary_key=True)
    active = models.BinaryField()
    courriel = models.CharField(max_length=255, blank=True, null=True)
    nom = models.CharField(max_length=255, blank=True, null=True)
    prenom = models.CharField(max_length=255, blank=True, null=True)
    telephone = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'employe'


class EvaluationArticle(models.Model):
    evaluation_article = models.ForeignKey('Intervention', primary_key=True)
    article = models.ForeignKey(Article, db_column='article_Id', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'evaluation_article'


class EvaluationLot(models.Model):
    evaluation_lot = models.ForeignKey('Intervention', primary_key=True)

    class Meta:
        managed = True
        db_table = 'evaluation_lot'


class EvaluationReception(models.Model):
    evaluation_reception = models.ForeignKey('Intervention', primary_key=True)

    class Meta:
        managed = True
        db_table = 'evaluation_reception'


class Expedition(models.Model):
    commande = models.CharField(max_length=255, blank=True, null=True)
    expedition = models.ForeignKey('Intervention', primary_key=True)

    class Meta:
        managed = True
        db_table = 'expedition'


class Fournisseur(models.Model):
    id = models.AutoField(primary_key=True)
    contact = models.CharField(max_length=255, blank=True, null=True)
    nom = models.CharField(max_length=255, blank=True, null=True)
    telephone = models.CharField(max_length=255, blank=True, null=True)
    web_site = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'fournisseur'


class Impression(models.Model):
    impression = models.ForeignKey('Intervention', primary_key=True)

    class Meta:
        managed = True
        db_table = 'impression'


class ImpressionCodeBarres(models.Model):
    impression_impression = models.ForeignKey(Impression, db_column='impression_Impression_id')  # Field name made lowercase.
    code_barres_texte = models.ForeignKey(CodeBarre, db_column='code_barres_texte', unique=True)

    class Meta:
        managed = True
        db_table = 'impression_code_barres'


class Intervention(models.Model):
    intervention_id = models.AutoField(primary_key=True)
    date = models.DateField(blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    reussie = models.TextField()  # This field type is a guess.
    employe = models.ForeignKey(Employe, db_column='employe_Id', blank=True, null=True)  # Field name made lowercase.
    lot = models.ForeignKey('Lot', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'intervention'


class Lot(models.Model):
    id = models.AutoField(primary_key=True)
    type_lot = models.IntegerField(blank=True, null=True)
    date_creation = models.DateField(blank=True, null=True)
    quantite_courante = models.IntegerField()
    quantite_initial = models.IntegerField()
    quantite_rejetee = models.IntegerField()
    quantite_testee = models.IntegerField()
    code_barre_texte = models.ForeignKey(CodeBarre, db_column='code_barre_texte', blank=True, null=True)
    employe = models.ForeignKey(Employe, db_column='employe_Id', blank=True, null=True)  # Field name made lowercase.
    produit = models.ForeignKey('Produit', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'lot'


class LotArticles(models.Model):
    lot_ar = models.ForeignKey(Lot, primary_key=True)
    production_production = models.ForeignKey('Production', db_column='production_Production_id', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'lot_articles'


class LotArticlesListArticles(models.Model):
    lot_articles_lot_ar = models.ForeignKey(LotArticles, db_column='lot_articles_lot_Ar_id')  # Field name made lowercase.
    list_articles = models.ForeignKey(Article, db_column='list_articles_Id', unique=True)  # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'lot_articles_list_articles'


class LotMatieresPremieres(models.Model):
    quantite_commande = models.IntegerField()
    lot_mp = models.ForeignKey(Lot, primary_key=True)
    evaluation_evaluationlot = models.ForeignKey(EvaluationLot, db_column='evaluation_EvaluationLot_id', blank=True, null=True)  # Field name made lowercase.
    reception_reception = models.ForeignKey('Reception', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'lot_matieres_premieres'


class Notification(models.Model):
    notification = models.ForeignKey(Intervention, primary_key=True)

    class Meta:
        managed = True
        db_table = 'notification'


class Production(models.Model):
    production = models.ForeignKey(Intervention, primary_key=True)

    class Meta:
        managed = True
        db_table = 'production'


class Produit(models.Model):
    id = models.AutoField(primary_key=True)
    code = models.CharField(max_length=255, blank=True, null=True)
    description_en = models.CharField(max_length=255, blank=True, null=True)
    description_fr = models.CharField(max_length=255, blank=True, null=True)
    nom_en = models.CharField(max_length=255, blank=True, null=True)
    nom_fr = models.CharField(max_length=255, blank=True, null=True)
    part_number = models.CharField(max_length=255, blank=True, null=True)
    quantite_stock = models.IntegerField()
    type_produit = models.CharField(max_length=255, blank=True, null=True)
    categorie_code = models.ForeignKey(Categorie, db_column='categorie_code', blank=True, null=True)
    fournisseur = models.ForeignKey(Fournisseur, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'produit'


class Reception(models.Model):
    reception = models.ForeignKey(Intervention, primary_key=True)

    class Meta:
        managed = True
        db_table = 'reception'
